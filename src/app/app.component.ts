import { AfterViewInit, Component, OnInit } from '@angular/core';
import Countdown from 'countdown-purejs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'usclient';

}
