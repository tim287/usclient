import { Component, OnInit } from '@angular/core';
import Countdown from 'countdown-purejs';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor() { }

  countdown: any;
  ngOnInit(): void {
    this.countdown = new Countdown("countdown", "2021-07-05:10-00", "french", "Bienvenu");
    this.countdown.createCountdown();
  }

}
